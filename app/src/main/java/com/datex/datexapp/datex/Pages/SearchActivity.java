package com.datex.datexapp.datex.Pages;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.datex.datexapp.datex.Adapters.JobLoader;
import com.datex.datexapp.datex.Adapters.PatientAdapter;
import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.R;
import com.datex.datexapp.datex.Widgets.ClickListener;
import com.datex.datexapp.datex.Widgets.RecyclerTouchListener;
import com.datex.datexapp.datex.Widgets.Utility;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Patient,Long> dao;
    public List<Patient> items;
    public Utility utils;

    public class MyViewHolder{
        public PatientAdapter mAdapter;
        public RecyclerView recyclerView;
        public List<Object> mRecyclerViewItems;
        public MyViewHolder(){
            recyclerView = findViewById(R.id.recycler_view);
            mRecyclerViewItems = new ArrayList<>();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            viewHolder = new MyViewHolder();
            dbService = new DbService(SearchActivity.this).getInstance(SearchActivity.this);
            dao = DaoManager.createDao(dbService.getConnectionSource(), Patient.class);
            utils = new Utility(SearchActivity.this);
            viewHolder.mAdapter = new PatientAdapter(viewHolder.mRecyclerViewItems);
            viewHolder.recyclerView = findViewById(R.id.recycler_view);
            //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.VERTICAL, false);
            viewHolder.recyclerView.setLayoutManager(layoutManager);
            viewHolder.recyclerView.setAdapter(viewHolder.mAdapter);
            viewHolder.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(SearchActivity.this, viewHolder.recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    JobLoader menuItem = (JobLoader)viewHolder.mRecyclerViewItems.get(position);
                    Patient item = menuItem.getPatient();
                    utils.getEditor().putLong("Id",item.getId()).commit();
                    startActivity(new Intent(SearchActivity.this,DetailActivity.class)
                            .putExtra("ID",item.getId()));
                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));
            loadFeeds();
        }catch (Exception e){}
    }
    public void loadFeeds() {
        try {
            /*QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
            queryBuilder.where().like("name",getIntent().getStringExtra("query"));*/
            items = dao.queryForAll();
            if(!items.isEmpty()) {
                for (int i = 0; i < items.size(); i++) {
                    if(items.get(i).getName().contains(getIntent().getStringExtra("query")) ||items.get(i).getReference().contains(getIntent().getStringExtra("query"))) {
                        JobLoader loader = new JobLoader(items.get(i), SearchActivity.this);
                        viewHolder.mRecyclerViewItems.add(loader);
                    }
                }
                if(viewHolder.mRecyclerViewItems.size()>0) {
                    findViewById(R.id.rela).setVisibility(View.GONE);
                    dao.clearObjectCache();
                    items.clear();
                }
            }
            viewHolder.mAdapter.notifyDataSetChanged();
        }catch(Exception e){}
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
