package com.datex.datexapp.datex.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.datex.datexapp.datex.Adapters.JobLoader;
import com.datex.datexapp.datex.Adapters.PatientAdapter;
import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.DbObjects.Results;
import com.datex.datexapp.datex.R;
import com.datex.datexapp.datex.Widgets.ClickListener;
import com.datex.datexapp.datex.Widgets.RecyclerTouchListener;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhysicalFragment extends Fragment {
    public View view;

    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Patient,Long> dao;
    public List<Patient> patients;
    public Dao<Results,Long> dao1;
    public List<Results> results;
    public Patient item;
    public Results item1;
    public String numbers="";
    public View ediView;

    public class MyViewHolder{
        public PatientAdapter mAdapter;
        public RecyclerView recyclerView;
        private FloatingActionButton fab;
        public List<Object> mRecyclerViewItems;
        public MyViewHolder(View view){
            recyclerView = view.findViewById(R.id.recycler_view);
            mRecyclerViewItems = new ArrayList<>();
            fab = view.findViewById(R.id.fab);
        }
    }


    public PhysicalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_hba, container, false);
        try{
            viewHolder = new MyViewHolder(view);
            dbService = new DbService(getContext()).getInstance(getContext());
            dao = DaoManager.createDao(dbService.getConnectionSource(), Patient.class);
            dao1 = DaoManager.createDao(dbService.getConnectionSource(), Results.class);
            viewHolder.mAdapter = new PatientAdapter(viewHolder.mRecyclerViewItems);
            viewHolder.recyclerView = view.findViewById(R.id.recycler_view);
            //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            viewHolder.recyclerView.setLayoutManager(layoutManager);
            viewHolder.recyclerView.setAdapter(viewHolder.mAdapter);
            viewHolder.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), viewHolder.recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {

                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));
            viewHolder.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i=0;i<viewHolder.mRecyclerViewItems.size();i++){
                        JobLoader loader = (JobLoader)viewHolder.mRecyclerViewItems.get(i);
                        Patient item = loader.getPatient();
                        numbers+=item.getPhone()+",";
                    }
                    ediView = LayoutInflater.from(getContext()).inflate(R.layout.message, null);
                    final EditText message = ediView.findViewById(R.id.message);
                    new AlertDialog.Builder(getContext())
                            .setTitle("Send Message")
                            .setView(ediView)
                            .setCancelable(false)
                            .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    try {
                                        new SendMessage().execute(message.getText().toString(),numbers.substring(0,(numbers.length()-1)));
                                    } catch (Exception e) {
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).show();
                }
            });
            loadFeeds();
            return view;
        }catch (Exception e){return null;}
    }
    public void loadFeeds() {
        try {
            patients = dao.queryForAll();
            if(!patients.isEmpty()) {
                for (Patient item2:patients) {
                    QueryBuilder<Results,Long> queryBuilder = dao1.queryBuilder().orderBy("Id",false);
                    queryBuilder.where().eq("patientId",item2.getId());
                    results = queryBuilder.query();
                    item1 = results.get(0);

                    long millis=System.currentTimeMillis();
                    java.sql.Date date=new java.sql.Date(millis);

                    Date dateTime = item1.getPhysicalTest();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dateTime);
                    calendar.add(Calendar.DAY_OF_YEAR,180);
                    dateTime = calendar.getTime();
                    if(date.after(dateTime)) {
                        JobLoader loader = new JobLoader(item2, getContext());
                        viewHolder.mRecyclerViewItems.add(loader);
                    }
                }
                if(viewHolder.mRecyclerViewItems.size()>0) {
                    view.findViewById(R.id.rela).setVisibility(View.GONE);
                    dao.clearObjectCache();
                    patients.clear();
                }
            }
            viewHolder.mAdapter.notifyDataSetChanged();
        }catch(Exception e){}
    }

    private class SendMessage extends AsyncTask<String, Void, String> {
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        String jsonObject = null;
        @Override
        protected String doInBackground(String... params){
            final String STRING_BASE_URL = "http://portal.bulksmsnigeria.net/api/";
            final String USER_PARAM = "username";
            final String PASS_PARAM = "password";
            final String MESSAGE = "message";
            final String SENDER = "sender";
            final String MOBILES = "mobiles";
            Uri builtUri = Uri.parse(STRING_BASE_URL).buildUpon()
                    .appendQueryParameter(USER_PARAM,"BlueCircleUNTH@gmail.com")
                    .appendQueryParameter(PASS_PARAM,"BlueCircleUNTH")
                    .appendQueryParameter(MESSAGE,params[0])
                    .appendQueryParameter(SENDER,"BCUNTH")
                    .appendQueryParameter(MOBILES,params[1])
                    .build();
            try{
                URL url = new URL(builtUri.toString());
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Cache-Control","no-cache");
                conn.setDefaultUseCaches(false);
                conn.setUseCaches(false);
                conn.setDoOutput(true);
                conn.connect();
                InputStream inputStream = conn.getInputStream();
                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                if (buffer.length() == 0) {
                    return null;
                }
                jsonObject = buffer.toString();
            }catch (IOException e){
            }
            finally {
                if (conn != null) {
                    conn.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return jsonObject;
        }
        @Override
        protected void onPostExecute(String result){
            if(result!=null){
                try{
                    JSONObject success = new JSONObject(result);
                    String message = success.getString("status");
                    if(message.equals("OK"))
                    {
                        Toast.makeText(getContext(), "Message Sent Successfully", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getContext(), "Message Send Failed", Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(getContext(),"No Internet Connectivity.",Toast.LENGTH_LONG).show();
            }
        }
    }

}
