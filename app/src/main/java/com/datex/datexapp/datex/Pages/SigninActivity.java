package com.datex.datexapp.datex.Pages;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.datex.datexapp.datex.R;

public class SigninActivity extends AppCompatActivity {
    public MyViewHolder viewHolder;

    public class MyViewHolder{
        private EditText user,pass;
        public MyViewHolder(){
            user = findViewById(R.id.username);
            pass = findViewById(R.id.password);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        viewHolder = new MyViewHolder();

    }
    public void login(View view){
        if(viewHolder.user.getText().toString().trim().equals("bluecircle") && viewHolder.pass.getText().toString().trim().equals("admincircle")){
            finish();
            startActivity(new Intent(SigninActivity.this,MainActivity.class));
        }else{
            Toast.makeText(this, "Username or Password Incorrect", Toast.LENGTH_LONG).show();
        }
    }
}
