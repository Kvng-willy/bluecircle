package com.datex.datexapp.datex.Pages;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.datex.datexapp.datex.Adapters.JobLoader;
import com.datex.datexapp.datex.Adapters.PatientAdapter;
import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.R;
import com.datex.datexapp.datex.Widgets.ClickListener;
import com.datex.datexapp.datex.Widgets.RecyclerTouchListener;
import com.datex.datexapp.datex.Widgets.Utility;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class WeeklyEntryActivity extends AppCompatActivity {
    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Patient,Long> dao;
    public List<Patient> items;
    public String numbers="";
    public Utility utils;

    public class MyViewHolder{
        public PatientAdapter mAdapter;
        public RecyclerView recyclerView;
        public List<Object> mRecyclerViewItems;
        private FloatingActionButton fab;
        public MyViewHolder(){
            recyclerView = findViewById(R.id.recycler_view);
            mRecyclerViewItems = new ArrayList<>();
            fab = findViewById(R.id.fab);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            viewHolder = new MyViewHolder();
            utils = new Utility(WeeklyEntryActivity.this);
            dbService = new DbService(WeeklyEntryActivity.this).getInstance(WeeklyEntryActivity.this);
            dao = DaoManager.createDao(dbService.getConnectionSource(), Patient.class);
            viewHolder.mAdapter = new PatientAdapter(viewHolder.mRecyclerViewItems);
            viewHolder.recyclerView = findViewById(R.id.recycler_view);
            //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(WeeklyEntryActivity.this, LinearLayoutManager.VERTICAL, false);
            viewHolder.recyclerView.setLayoutManager(layoutManager);
            viewHolder.recyclerView.setAdapter(viewHolder.mAdapter);
            viewHolder.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(WeeklyEntryActivity.this, viewHolder.recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    JobLoader menuItem = (JobLoader)viewHolder.mRecyclerViewItems.get(position);
                    Patient item = menuItem.getPatient();
                    utils.getEditor().putLong("Id",item.getId()).commit();
                    startActivity(new Intent(WeeklyEntryActivity.this,DetailActivity.class)
                            .putExtra("ID",item.getId()));
                }
                @Override
                public void onLongClick(View view, int position) { }
            }));
            viewHolder.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i=0;i<viewHolder.mRecyclerViewItems.size();i++){
                        JobLoader loader = (JobLoader)viewHolder.mRecyclerViewItems.get(i);
                        Patient item = loader.getPatient();
                        numbers+=item.getPhone()+",";
                    }
                    File filePath = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/Contacts/");
                    if(!filePath.exists()) {
                        filePath.mkdirs();
                    }
                    try
                    {
                        long millis=System.currentTimeMillis();
                        java.sql.Date date=new java.sql.Date(millis);
                        File file = new File(filePath,date.toString()+"numbers.txt");
                        if(!file.exists()) {
                            file.createNewFile();
                            FileOutputStream fOut = new FileOutputStream(file);
                            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                            myOutWriter.append(numbers.substring(0,(numbers.length()-1)));
                            myOutWriter.close();
                            fOut.flush();
                            fOut.close();
                        }
                        Toast.makeText(WeeklyEntryActivity.this, "Contacts downloaded successfully", Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e) {
                        Toast.makeText(WeeklyEntryActivity.this, "Cannot export contacts at this moment. Please retry", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            viewHolder.fab.setVisibility(View.GONE);
            loadFeeds();
        }catch (Exception e){}
    }
    public void loadFeeds() {
        try {
            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder().orderBy("Id",false);
            items = queryBuilder.query();
            if(!items.isEmpty()) {
                for (int i = 0; i < items.size(); i++) {
                        long millis=System.currentTimeMillis();
                        java.sql.Date date=new java.sql.Date(millis);
                        Date dateTime = items.get(i).getDate();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(dateTime);
                        calendar.add(Calendar.DAY_OF_YEAR,7);
                        dateTime = calendar.getTime();
                    if(dateTime.after(date)) {
                        JobLoader loader = new JobLoader(items.get(i), WeeklyEntryActivity.this);
                        viewHolder.mRecyclerViewItems.add(loader);
                    }
                }
                if(viewHolder.mRecyclerViewItems.size()>0) {
                    findViewById(R.id.rela).setVisibility(View.GONE);
                    viewHolder.fab.setVisibility(View.VISIBLE);
                    dao.clearObjectCache();
                    items.clear();
                }
            }
            viewHolder.mAdapter.notifyDataSetChanged();
        }catch(Exception e){}
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
