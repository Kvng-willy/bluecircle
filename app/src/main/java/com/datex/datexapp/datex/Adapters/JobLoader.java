package com.datex.datexapp.datex.Adapters;

import android.content.Context;

import com.datex.datexapp.datex.DbObjects.Patient;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * Created by KVNG WILLY on 27/09/2017.
 */
public class JobLoader {
    private Patient patient;
    private Context mContext;

    public JobLoader(Patient item, Context context) {
        this.patient = item;
        this.mContext = context;
    }

    public Patient getPatient() {
        return patient;
    }

    public Context getmContext(){return mContext;}

}
