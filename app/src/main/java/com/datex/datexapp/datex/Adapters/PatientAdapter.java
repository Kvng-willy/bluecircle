package com.datex.datexapp.datex.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.R;

import java.util.List;

/**
 * Created by KVNG_WILLY on 05/03/2018.
 */

public class PatientAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public Context mContext;
    private final List<Object> mRecyclerViewItems;
    public int count = -1;
    public int[] draw= {android.R.color.holo_blue_dark,android.R.color.holo_green_dark,android.R.color.holo_orange_dark,android.R.color.holo_red_dark};

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,age,number,reference,bp,medic,diagnosis;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            age = view.findViewById(R.id.age);
            number = view.findViewById(R.id.number);
            reference = view.findViewById(R.id.reference);
        }
    }
    public PatientAdapter(List<Object> recyclerViewItems) {
        this.mRecyclerViewItems = recyclerViewItems;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.patient_hold, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        try{
            MyViewHolder menuItemHolder = (MyViewHolder) holder;
            JobLoader menuItem = (JobLoader)mRecyclerViewItems.get(position);
            mContext = menuItem.getmContext();
            Patient item = menuItem.getPatient();
            menuItemHolder.name.setText(item.getName());
            menuItemHolder.age.setText(item.getAge()+" Years");
            menuItemHolder.number.setText(item.getPhone());
            menuItemHolder.reference.setText("#"+item.getReference());


        }catch (Exception e){}
    }

    @Override
    public int getItemCount() {
        return mRecyclerViewItems.size();
    }
}
