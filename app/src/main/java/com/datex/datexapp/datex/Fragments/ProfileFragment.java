package com.datex.datexapp.datex.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.Pages.DetailActivity;
import com.datex.datexapp.datex.R;
import com.datex.datexapp.datex.Widgets.Utility;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    public View view;
    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Patient,Long> dao;
    public List<Patient> items;
    public Patient item;
    public Utility utils;
    String myFormat = "E dd-MMM-yyyy";
    SimpleDateFormat sdf;


    public ProfileFragment() {
        // Required empty public constructor
    }

    public class MyViewHolder{
        public EditText name,age,number,bmi,rbg,medic,bp,gender,diagnosis,entry,dateDiag;
        public Button submit;
        public MyViewHolder(View view){
            name = view.findViewById(R.id.name);
            age = view.findViewById(R.id.age);
            number = view.findViewById(R.id.number);
            rbg = view.findViewById(R.id.rbg);
            bmi = view.findViewById(R.id.bmi);
            medic = view.findViewById(R.id.medic);
            bp = view.findViewById(R.id.bp);
            gender = view.findViewById(R.id.gender);
            diagnosis = view.findViewById(R.id.diagnosis);
            submit = view.findViewById(R.id.submit);
            entry = view.findViewById(R.id.entry);
            dateDiag = view.findViewById(R.id.diag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_profile, container, false);
        try{
            utils = new Utility(getContext());
            sdf = new SimpleDateFormat(myFormat, Locale.US);
            dbService = new DbService(getContext()).getInstance(getContext());
            dao = DaoManager.createDao(dbService.getConnectionSource(), Patient.class);
            viewHolder = new MyViewHolder(view);
            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
            queryBuilder.where().like("Id",utils.getPref().getLong("Id",0));
            items = queryBuilder.query();
            item = items.get(0);
            viewHolder.name.setText(item.getName());
            viewHolder.age.setText(item.getAge());
            viewHolder.number.setText(item.getPhone());
            viewHolder.rbg.setText(item.getRbg());
            viewHolder.bmi.setText(item.getBmi());
            viewHolder.medic.setText(item.getMedic());
            viewHolder.bp.setText(item.getBp());
            viewHolder.gender.setText(item.getGender());
            viewHolder.diagnosis.setText(item.getDiagnosis());
            viewHolder.entry.setText(sdf.format(item.getDate()));
            viewHolder.dateDiag.setText(sdf.format(item.getDateOfDiagnosis()));
            viewHolder.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.submit.setVisibility(View.GONE);
                    //view.findViewById(R.id.avi).setVisibility(View.VISIBLE);
                    ValidateFields();
                    viewHolder.submit.setVisibility(View.VISIBLE);
                    //view.findViewById(R.id.avi).setVisibility(View.GONE);
                }
            });
            return view;
        }catch (Exception e){ return  null;}
    }
    public void ValidateFields(){
        try {
            if (viewHolder.name.getText().toString().isEmpty()) {
                viewHolder.name.setError("Field Cannot Be Empty");
            } else if (viewHolder.age.getText().toString().isEmpty()) {
                viewHolder.age.setError("Field Cannot Be Empty");
            } else if (viewHolder.number.getText().toString().isEmpty()) {
                viewHolder.number.setError("Field Cannot Be Empty");
            } else if (viewHolder.bmi.getText().toString().isEmpty()) {
                viewHolder.bmi.setError("Field Cannot Be Empty");
            } else if (viewHolder.rbg.getText().toString().isEmpty()) {
                viewHolder.rbg.setError("Field Cannot Be Empty");
            } else if (viewHolder.medic.getText().toString().isEmpty()) {
                viewHolder.medic.setError("Field Cannot Be Empty");
            }  else if (viewHolder.bp.getText().toString().isEmpty()) {
                viewHolder.bp.setError("Field Cannot Be Empty");
            } else {
                long millis=System.currentTimeMillis();
                java.sql.Date date=new java.sql.Date(millis);

                item.setName(viewHolder.name.getText().toString());
                item.setAge(viewHolder.age.getText().toString());
                item.setGender(viewHolder.gender.getText().toString());
                item.setPhone(viewHolder.number.getText().toString());
                item.setBmi(viewHolder.bmi.getText().toString());
                item.setRbg(viewHolder.rbg.getText().toString());
                item.setMedic(viewHolder.medic.getText().toString());
                item.setBp(viewHolder.bp.getText().toString());
                item.setDiagnosis(viewHolder.diagnosis.getText().toString());
                item.setDate(date);
                dao.update(item);
                Toast.makeText(getContext(), "Patient Profile Updated", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){}
    }
}
