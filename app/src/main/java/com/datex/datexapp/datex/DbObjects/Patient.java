package com.datex.datexapp.datex.DbObjects;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

/**
 * Created by KVNG_WILLY on 06/03/2018.
 */

public class Patient {
    @DatabaseField(generatedId = true)
    public long Id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String age;
    @DatabaseField
    public String gender;
    @DatabaseField
    private String phone;
    @DatabaseField
    private String bmi;
    @DatabaseField
    private String rbg;
    @DatabaseField
    private String medic;
    @DatabaseField
    private String bp;
    @DatabaseField
    private Date date;
    @DatabaseField
    private String diagnosis;
    @DatabaseField
    private String reference;
    @DatabaseField
    private Date dateOfDiagnosis;


    public long getId(){
        return Id;
    }
    public void setId(long id)
    {
        this.Id = id;
    }
    public String getName(){
        return name;
    }
    public void setName(String id)
    {
        this.name = id;
    }
    public String getAge(){
        return age;
    }
    public void setAge(String id)
    {
        this.age = id;
    }
    public String getGender(){
        return gender;
    }
    public void setGender(String name)
    {
        this.gender = name;
    }
    public String getPhone(){
        return phone;
    }
    public void setPhone(String name)
    {
        this.phone = name;
    }
    public String getBmi(){
        return bmi;
    }
    public void setBmi(String name)
    {
        this.bmi = name;
    }
    public String getRbg(){
        return rbg;
    }
    public void setRbg(String name)
    {
        this.rbg = name;
    }
    public String getMedic(){
        return medic;
    }
    public void setMedic(String name)
    {
        this.medic = name;
    }
    public String getBp(){
        return bp;
    }
    public void setBp(String name)
    {
        this.bp = name;
    }
    public String getDiagnosis(){
        return diagnosis;
    }
    public void setDiagnosis(String name)
    {
        this.diagnosis = name;
    }
    public String getReference(){
        return reference;
    }
    public void setReference(String name)
    {
        this.reference = name;
    }
    public Date getDate(){
        return date;
    }
    public void setDate(Date name)
    {
        this.date = name;
    }
    public Date getDateOfDiagnosis(){
        return dateOfDiagnosis;
    }
    public void setDateOfDiagnosis(Date name)
    {
        this.dateOfDiagnosis = name;
    }
}
