package com.datex.datexapp.datex.Pages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.R;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MessageActivity extends AppCompatActivity {
    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Patient,Long> dao;
    public List<Patient> items;
    public String numberGet="";
    public String numbers="";

    public class MyViewHolder{
        public RadioGroup radioGroup;
        private Button submit;
        private EditText message;
        public MyViewHolder(){
            radioGroup = findViewById(R.id.messageGroup);
            submit = findViewById(R.id.submit);
            message = findViewById(R.id.message);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        try {
            viewHolder = new MyViewHolder();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Send Message");
            dbService = new DbService(MessageActivity.this).getInstance(MessageActivity.this);
            dao = DaoManager.createDao(dbService.getConnectionSource(), Patient.class);
            viewHolder.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int id = viewHolder.radioGroup.getCheckedRadioButtonId();
                        if (id == R.id.allpatients) {
                            numberGet="";
                            items = dao.queryForAll();
                            for(Patient item:items){
                                numberGet+=item.getPhone()+",";
                            }
                            numbers = numberGet.substring(0,(numberGet.length()-1));
                        }
                        if (id == R.id.gdm) {
                            numberGet="";
                            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
                            queryBuilder.where().eq("diagnosis","Gestational DM");
                            items = queryBuilder.query();
                            for(Patient item:items){
                                numberGet+=item.getPhone()+",";
                            }
                            numbers = numberGet.substring(0,(numberGet.length()-1));
                        }
                        if (id == R.id.male) {
                            numberGet="";
                            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
                            queryBuilder.where().eq("gender","Male");
                            items = queryBuilder.query();
                            for(Patient item:items){
                                numberGet+=item.getPhone()+",";
                            }
                            numbers = numberGet.substring(0,(numberGet.length()-1));
                        }
                        if (id == R.id.female) {
                            numberGet="";
                            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
                            queryBuilder.where().eq("gender","Female");
                            items = queryBuilder.query();
                            for(Patient item:items){
                                numberGet+=item.getPhone()+",";
                            }
                            numbers = numberGet.substring(0,(numberGet.length()-1));
                        }
                        if(!numbers.isEmpty()) {
                            new AlertDialog.Builder(MessageActivity.this)
                                    .setTitle("Send Message")
                                    .setMessage("Please Confirm Message Send")
                                    .setCancelable(false)
                                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            try {
                                                new SendMessage().execute(viewHolder.message.getText().toString(),numbers);
                                            } catch (Exception e) {
                                            }
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    }).show();
                        }else{
                            Toast.makeText(MessageActivity.this, "Please Message Send Group", Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){}
                }
            });
        }catch (Exception e){}

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private class SendMessage extends AsyncTask<String, Void, String> {
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        String jsonObject = null;
        @Override
        protected String doInBackground(String... params){
            final String STRING_BASE_URL = "http://portal.bulksmsnigeria.net/api/";
            final String USER_PARAM = "username";
            final String PASS_PARAM = "password";
            final String MESSAGE = "message";
            final String SENDER = "sender";
            final String MOBILES = "mobiles";
            Uri builtUri = Uri.parse(STRING_BASE_URL).buildUpon()
                    .appendQueryParameter(USER_PARAM,"BlueCircleUNTH@gmail.com")
                    .appendQueryParameter(PASS_PARAM,"BlueCircleUNTH")
                    .appendQueryParameter(MESSAGE,params[0])
                    .appendQueryParameter(SENDER,"BCUNTH")
                    .appendQueryParameter(MOBILES,params[1])
                    .build();
            try{
                URL url = new URL(builtUri.toString());
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Cache-Control","no-cache");
                conn.setDefaultUseCaches(false);
                conn.setUseCaches(false);
                conn.setDoOutput(true);
                conn.connect();
                InputStream inputStream = conn.getInputStream();
                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                if (buffer.length() == 0) {
                    return null;
                }
                jsonObject = buffer.toString();
            }catch (IOException e){
            }
            finally {
                if (conn != null) {
                    conn.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return jsonObject;
        }
        @Override
        protected void onPostExecute(String result){
            if(result!=null){
                try{
                    JSONObject success = new JSONObject(result);
                    String message = success.getString("status");
                    if(message.equals("OK"))
                    {
                        Toast.makeText(MessageActivity.this, "Message Sent Successfully", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    Toast.makeText(MessageActivity.this, "Message Send Failed", Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(MessageActivity.this,"No Internet Connectivity.",Toast.LENGTH_LONG).show();
            }
        }
    }
}
