package com.datex.datexapp.datex.DbObjects;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

/**
 * Created by KVNG_WILLY on 06/03/2018.
 */

public class Results {
    @DatabaseField(generatedId = true)
    public long Id;
    @DatabaseField
    private String patientId;
    @DatabaseField
    private String hba1;
    @DatabaseField
    private String choles;
    @DatabaseField
    private String trig;
    @DatabaseField
    private String hdl;
    @DatabaseField
    private String ldl;
    @DatabaseField
    private Date dateHba;
    @DatabaseField
    private Date dateOther;
    @DatabaseField
    private Date physicalTest;


    public long getId(){
        return Id;
    }
    public void setId(long id)
    {
        this.Id = id;
    }
    public String getPatientId(){
        return patientId;
    }
    public void setPatientId(String id)
    {
        this.patientId = id;
    }
    public Date getDateHba(){
        return dateHba;
    }
    public void setDateHba(Date id)
    {
        this.dateHba = id;
    }
    public String getHba1(){
        return hba1;
    }
    public void setHba1(String name)
    {
        this.hba1 = name;
    }
    public String getTrig(){
        return trig;
    }
    public void setTrig(String name)
    {
        this.trig = name;
    }
    public String getCholes(){
        return choles;
    }
    public void setCholes(String name)
    {
        this.choles = name;
    }
    public String getHdl(){
        return hdl;
    }
    public void setHdl(String name)
    {
        this.hdl = name;
    }
    public String getLdl(){
        return ldl;
    }
    public void setLdl(String name)
    {
        this.ldl = name;
    }
    public Date getDateOther(){
        return dateOther;
    }
    public void setDateOther(Date name)
    {
        this.dateOther = name;
    }
    public Date getPhysicalTest(){
        return physicalTest;
    }
    public void setPhysicalTest(Date name)
    {
        this.physicalTest = name;
    }
}
