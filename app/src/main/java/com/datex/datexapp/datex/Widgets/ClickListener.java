package com.datex.datexapp.datex.Widgets;

import android.view.View;

/**
 * Created by KVNG_WILLY on 05/03/2018.
 */

public interface ClickListener{
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
