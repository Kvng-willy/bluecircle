package com.datex.datexapp.datex.Widgets;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Created by KVNG_WILLY on 21/03/2018.
 */

public class Utility {
    public Context context;
    public SharedPreferences.Editor editor;

    public Utility(Context c)
    {
        context = c;
    }

    public SharedPreferences getPref()
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref;
    }
    public SharedPreferences.Editor getEditor()
    {
        SharedPreferences pref = getPref();
        editor = pref.edit();
        return editor;
    }

    public void AlertToastNegative(Context e,String Message){
        Toast.makeText(e, Message, Toast.LENGTH_SHORT).show();
    }
    public String removeChar(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ']') {
            str = str.substring(1, str.length() - 1);
        }
        return str;
    }


}
