package com.datex.datexapp.datex.Fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.DbObjects.Results;
import com.datex.datexapp.datex.Pages.AddPatientActivity;
import com.datex.datexapp.datex.R;
import com.datex.datexapp.datex.Widgets.Utility;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends Fragment {

    public View view;
    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Results,Long> dao;
    public List<Results> items;
    public Results item;
    public Utility utils;
    public int count = 0;
    public View ediView;
    String myFormat = "E dd-MMM-yyyy";
    String myFormat1 = "yyyy/MM/dd";
    SimpleDateFormat sdf;
    SimpleDateFormat sdf1;
    Calendar myCalendar;
    Calendar myCalendar1;
    Calendar myCalendar2;

    public ResultFragment() {
        // Required empty public constructor
    }

    public class MyViewHolder{
        public LinearLayout testLayout;
        public Button newResult;
        public MyViewHolder(View view){
            testLayout = view.findViewById(R.id.testLayout);
            newResult = view.findViewById(R.id.newResult);
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_result, container, false);
        try{
            sdf = new SimpleDateFormat(myFormat, Locale.US);
            sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
            myCalendar = Calendar.getInstance();
            myCalendar1 = Calendar.getInstance();
            myCalendar2 = Calendar.getInstance();
            utils = new Utility(getContext());
            dbService = new DbService(getContext()).getInstance(getContext());
            dao = DaoManager.createDao(dbService.getConnectionSource(), Results.class);
            viewHolder = new MyViewHolder(view);
            StartView();
            viewHolder.newResult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        ediView = LayoutInflater.from(getContext()).inflate(R.layout.add_test, null);
                        final Results item1 = new Results();
                        final EditText hba = ediView.findViewById(R.id.hba);
                        final EditText physical = ediView.findViewById(R.id.physical);
                        final EditText hdl = ediView.findViewById(R.id.hdl);
                        final EditText choles = ediView.findViewById(R.id.choles);
                        final EditText trig = ediView.findViewById(R.id.trig);
                        final EditText ldl = ediView.findViewById(R.id.ldl);
                        final EditText date = ediView.findViewById(R.id.Birthday);
                        final EditText date1 = ediView.findViewById(R.id.Birthday1);
                        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                myCalendar.set(Calendar.YEAR, year);
                                myCalendar.set(Calendar.MONTH, monthOfYear);
                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                date.setText(sdf1.format(myCalendar.getTime()));
                            }

                        };
                        final DatePickerDialog.OnDateSetListener date3 = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                myCalendar1.set(Calendar.YEAR, year);
                                myCalendar1.set(Calendar.MONTH, monthOfYear);
                                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                date1.setText(sdf1.format(myCalendar1.getTime()));
                            }

                        };
                        final DatePickerDialog.OnDateSetListener date4 = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                myCalendar2.set(Calendar.YEAR, year);
                                myCalendar2.set(Calendar.MONTH, monthOfYear);
                                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                physical.setText(sdf1.format(myCalendar2.getTime()));
                            }

                        };
                        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if (b) {
                                    new DatePickerDialog(getContext(), date2, myCalendar
                                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                                }
                            }
                        });

                        date1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if (b) {
                                    new DatePickerDialog(getContext(), date3, myCalendar1
                                            .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                                            myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
                                }
                            }
                        });
                        physical.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if (b) {
                                    new DatePickerDialog(getContext(), date4, myCalendar2
                                            .get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH),
                                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                                }
                            }
                        });
                        new AlertDialog.Builder(getContext())
                                .setView(ediView)
                                .setCancelable(false)
                                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        try {
                                            long dateHba1 = myCalendar.getTimeInMillis();
                                            Date dateHba = new Date(dateHba1);
                                            long dateOth = myCalendar1.getTimeInMillis();
                                            Date dateOther = new Date(dateOth);
                                            long datePh = myCalendar.getTimeInMillis();
                                            Date datePhy = new Date(datePh);

                                            item1.setHba1(hba.getText().toString()+"%");
                                            item1.setCholes(choles.getText().toString()+"mmol/L");
                                            item1.setHdl(hdl.getText().toString()+"mmol/L");
                                            item1.setLdl(ldl.getText().toString()+"mmol/L");
                                            item1.setTrig(trig.getText().toString()+"mmol/L");
                                            item1.setDateHba(dateHba);
                                            item1.setDateOther(dateOther);
                                            item1.setPhysicalTest(datePhy);
                                            item1.setPatientId(Integer.toString(utils.getPref().getInt("Id",0)));
                                            dao.create(item1);
                                            StartView();
                                            Toast.makeText(getContext(), "New Test Result Added Successfully", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();

                    } catch (Exception e) { }
                }
            });
            return view;
        }catch (Exception e){return view;}
    }
    public void StartView(){
        try{
            viewHolder.testLayout.removeAllViews();
            count=0;
            QueryBuilder<Results,Long> queryBuilder = dao.queryBuilder().orderBy("Id",false);
            queryBuilder.where().like("patientId",utils.getPref().getLong("Id",0));
            items = queryBuilder.query();
            for(Results item:items) {
                PopulateResult(item,count+=1);
            }
        }catch (Exception e){}
    }
    public void PopulateResult(Results item,int count){
        try{
            LayoutInflater inflater = (LayoutInflater) getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view1 = inflater.inflate(R.layout.test_hold, null);
            TextView name = view1.findViewById(R.id.testTitle);
            final TextView nameId = view1.findViewById(R.id.testid);
            TextView hbValue = view1.findViewById(R.id.hbValue);
            TextView hbDate = view1.findViewById(R.id.hbDate);
            TextView hdValue = view1.findViewById(R.id.hdValue);
            TextView hdDate = view1.findViewById(R.id.hdDate);
            TextView chValue = view1.findViewById(R.id.chValue);
            TextView chDate = view1.findViewById(R.id.chDate);
            TextView trigValue = view1.findViewById(R.id.trigValue);
            TextView trigDate = view1.findViewById(R.id.trigDate);
            TextView ldValue = view1.findViewById(R.id.ldValue);
            TextView ldDate = view1.findViewById(R.id.ldDate);
            TextView physical = view1.findViewById(R.id.physical);
            Button edit = view1.findViewById(R.id.edit);
            name.setText("Test Result "+Integer.toString(count));
            hbValue.setText(item.getHba1());
            hbDate.setText(sdf.format(item.getDateHba()));
            hdValue.setText(item.getHdl());
            ldValue.setText(item.getLdl());
            chValue.setText(item.getCholes());
            chDate.setText(sdf.format(item.getDateOther()));
            trigValue.setText(item.getTrig());
            trigDate.setText(sdf.format(item.getDateOther()));
            hdDate.setText(sdf.format(item.getDateOther()));
            ldDate.setText(sdf.format(item.getDateOther()));
            physical.setText(sdf.format(item.getPhysicalTest()));
            nameId.setText(Long.toString(item.getId()));
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        ediView = LayoutInflater.from(getContext()).inflate(R.layout.add_test, null);
                        QueryBuilder<Results, Long> queryBuilder = dao.queryBuilder().orderBy("Id", false);
                        queryBuilder.where().like("Id", Long.parseLong(nameId.getText().toString()));
                        items = queryBuilder.query();
                        final Results item1 = items.get(0);
                        final EditText hba = ediView.findViewById(R.id.hba);
                        final EditText physical = ediView.findViewById(R.id.physical);
                        final EditText hdl = ediView.findViewById(R.id.hdl);
                        final EditText choles = ediView.findViewById(R.id.choles);
                        final EditText trig = ediView.findViewById(R.id.trig);
                        final EditText ldl = ediView.findViewById(R.id.ldl);
                        final EditText date = ediView.findViewById(R.id.Birthday);
                        final EditText date1 = ediView.findViewById(R.id.Birthday1);
                        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                myCalendar.set(Calendar.YEAR, year);
                                myCalendar.set(Calendar.MONTH, monthOfYear);
                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                date.setText(sdf1.format(myCalendar.getTime()));
                            }

                        };
                        final DatePickerDialog.OnDateSetListener date3 = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                myCalendar1.set(Calendar.YEAR, year);
                                myCalendar1.set(Calendar.MONTH, monthOfYear);
                                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                date1.setText(sdf1.format(myCalendar1.getTime()));
                            }

                        };
                        final DatePickerDialog.OnDateSetListener date4 = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                myCalendar2.set(Calendar.YEAR, year);
                                myCalendar2.set(Calendar.MONTH, monthOfYear);
                                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                physical.setText(sdf1.format(myCalendar2.getTime()));
                            }

                        };
                        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if(b){
                                    new DatePickerDialog(getContext(), date2, myCalendar
                                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                                }
                            }
                        });

                        date1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if(b){
                                    new DatePickerDialog(getContext(), date3, myCalendar1
                                            .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                                            myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
                                }
                            }
                        });
                        physical.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if(b){
                                    new DatePickerDialog(getContext(), date4, myCalendar2
                                            .get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH),
                                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                                }
                            }
                        });

                        hba.setText(item1.getHba1());
                        physical.setText(sdf1.format(item1.getPhysicalTest()));
                        hdl.setText(item1.getHdl());
                        ldl.setText(item1.getLdl());
                        trig.setText(item1.getTrig());
                        choles.setText(item1.getCholes());
                        date.setText(sdf1.format(item1.getDateHba()));
                        date1.setText(sdf1.format(item1.getDateOther()));
                        new AlertDialog.Builder(getContext())
                                .setView(ediView)
                                .setCancelable(false)
                                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        try {
                                            long dateHba1=myCalendar.getTimeInMillis();
                                            Date dateHba=new Date(dateHba1);
                                            long dateOth=myCalendar1.getTimeInMillis();
                                            Date dateOther=new Date(dateOth);
                                            long datePh=myCalendar.getTimeInMillis();
                                            Date datePhy=new Date(datePh);

                                            item1.setHba1(hba.getText().toString());
                                            item1.setCholes(choles.getText().toString());
                                            item1.setHdl(hdl.getText().toString());
                                            item1.setLdl(ldl.getText().toString());
                                            item1.setTrig(trig.getText().toString());
                                            item1.setDateHba(dateHba);
                                            item1.setDateOther(dateOther);
                                            item1.setPhysicalTest(datePhy);
                                            dao.update(item1);
                                            StartView();
                                            Toast.makeText(getContext(), "Update Successful", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();

                    } catch (Exception e) {Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show(); }
                }
            });
            viewHolder.testLayout.addView(view1);
        }catch (Exception e){
            Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

}
