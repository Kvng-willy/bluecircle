package com.datex.datexapp.datex.Pages;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.DbObjects.Results;
import com.datex.datexapp.datex.R;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddPatientActivity extends AppCompatActivity {
    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Patient,Long> dao;
    public Patient item;
    public Dao<Results,Long> dao1;
    public Results item1;
    Calendar myCalendar;
    Calendar myCalendar1;
    Calendar myCalendar2;
    Calendar myCalendar3;
    String myFormat = "dd/MM/yy";
    SimpleDateFormat sdf;

    public class MyViewHolder{
        public EditText name,age,number,bmi,rbg,medic,hba,choles,hdl,ldl,bp,date,date1,physical,trig,dateDiag;
        public Spinner gender,diagnosis;
        public Button submit;
        public MyViewHolder(){
            name = findViewById(R.id.name);
            age = findViewById(R.id.age);
            number = findViewById(R.id.number);
            rbg = findViewById(R.id.rbg);
            bmi = findViewById(R.id.bmi);
            medic = findViewById(R.id.medic);
            hba = findViewById(R.id.hba);
            choles = findViewById(R.id.choles);
            hdl = findViewById(R.id.hdl);
            ldl = findViewById(R.id.ldl);
            bp = findViewById(R.id.bp);
            gender = findViewById(R.id.gender);
            submit = findViewById(R.id.submit);
            diagnosis = findViewById(R.id.diagnosis);
            date = findViewById(R.id.Birthday);
            date1 = findViewById(R.id.Birthday1);
            physical = findViewById(R.id.physical);
            trig = findViewById(R.id.trig);
            dateDiag = findViewById(R.id.Birthday2);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            viewHolder = new MyViewHolder();
            dbService = new DbService(AddPatientActivity.this).getInstance(AddPatientActivity.this);
            dao = DaoManager.createDao(dbService.getConnectionSource(),Patient.class);
            dao1 = DaoManager.createDao(dbService.getConnectionSource(),Results.class);
            myCalendar = Calendar.getInstance();
            myCalendar1 = Calendar.getInstance();
            myCalendar2 = Calendar.getInstance();
            myCalendar3 = Calendar.getInstance();
            sdf = new SimpleDateFormat(myFormat, Locale.US);
            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel();
                }

            };
            final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    myCalendar1.set(Calendar.YEAR, year);
                    myCalendar1.set(Calendar.MONTH, monthOfYear);
                    myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel1();
                }

            };
            final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    myCalendar2.set(Calendar.YEAR, year);
                    myCalendar2.set(Calendar.MONTH, monthOfYear);
                    myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel2();
                }

            };
            final DatePickerDialog.OnDateSetListener date3 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    myCalendar3.set(Calendar.YEAR, year);
                    myCalendar3.set(Calendar.MONTH, monthOfYear);
                    myCalendar3.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel3();
                }

            };
            viewHolder.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.submit.setVisibility(View.GONE);
                    findViewById(R.id.avi).setVisibility(View.VISIBLE);
                    ValidateFields();
                    viewHolder.submit.setVisibility(View.VISIBLE);
                    findViewById(R.id.avi).setVisibility(View.GONE);
                }
            });
            viewHolder.date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b){
                        new DatePickerDialog(AddPatientActivity.this, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                }
            });

            viewHolder.date1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b){
                        new DatePickerDialog(AddPatientActivity.this, date1, myCalendar1
                                .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                                myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
                    }
                }
            });
            viewHolder.physical.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b){
                        new DatePickerDialog(AddPatientActivity.this, date2, myCalendar2
                                .get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH),
                                myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                    }
                }
            });
            viewHolder.dateDiag.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b){
                        new DatePickerDialog(AddPatientActivity.this, date3, myCalendar3
                                .get(Calendar.YEAR), myCalendar3.get(Calendar.MONTH),
                                myCalendar3.get(Calendar.DAY_OF_MONTH)).show();
                    }
                }
            });
        }catch (Exception e){}

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void ValidateFields(){
        try {
            if (viewHolder.name.getText().toString().isEmpty()) {
                viewHolder.name.setError("Field Cannot Be Empty");
            } else if (viewHolder.age.getText().toString().isEmpty()) {
                viewHolder.age.setError("Field Cannot Be Empty");
            } else if (viewHolder.gender.getSelectedItem().toString().equals("--Select Gender--")) {

                Toast.makeText(this, "Please Select Gender", Toast.LENGTH_SHORT).show();
            } else if (viewHolder.number.getText().toString().isEmpty()) {
                viewHolder.number.setError("Field Cannot Be Empty");
            } else if (viewHolder.bmi.getText().toString().isEmpty()) {
                viewHolder.bmi.setError("Field Cannot Be Empty");
            } else if (viewHolder.rbg.getText().toString().isEmpty()) {
                viewHolder.rbg.setError("Field Cannot Be Empty");
            } else if (viewHolder.medic.getText().toString().isEmpty()) {
                viewHolder.medic.setError("Field Cannot Be Empty");
            } else if (viewHolder.hba.getText().toString().isEmpty()) {
                viewHolder.hba.setError("Field Cannot Be Empty");
            } else if (viewHolder.choles.getText().toString().isEmpty()) {
                viewHolder.choles.setError("Field Cannot Be Empty");
            } else if (viewHolder.hdl.getText().toString().isEmpty()) {
                viewHolder.hdl.setError("Field Cannot Be Empty");
            } else if (viewHolder.ldl.getText().toString().isEmpty()) {
                viewHolder.ldl.setError("Field Cannot Be Empty");
            } else if (viewHolder.bp.getText().toString().isEmpty()) {
                viewHolder.bp.setError("Field Cannot Be Empty");
            } else if(viewHolder.diagnosis.getSelectedItem().toString().equals("--Select Type--")){
                Toast.makeText(this, "Please Select Type", Toast.LENGTH_SHORT).show();
            }else if (viewHolder.trig.getText().toString().isEmpty()) {
                viewHolder.trig.setError("Field Cannot Be Empty");
            }else {
                long millis=System.currentTimeMillis();
                java.sql.Date date=new java.sql.Date(millis);
                long datediag=myCalendar3.getTimeInMillis();
                java.sql.Date dateDiag=new java.sql.Date(datediag);

                item = new Patient();
                item1 = new Results();
                item.setName(viewHolder.name.getText().toString());
                item.setAge(viewHolder.age.getText().toString());
                item.setGender(viewHolder.gender.getSelectedItem().toString());
                item.setPhone(viewHolder.number.getText().toString());
                item.setBmi(viewHolder.bmi.getText().toString()+"Kg/m2");
                item.setRbg(viewHolder.rbg.getText().toString()+"mmol/L");
                item.setMedic(viewHolder.medic.getText().toString());
                item.setBp(viewHolder.bp.getText().toString()+"mmHg");
                item.setDiagnosis(viewHolder.diagnosis.getSelectedItem().toString());
                item.setDate(date);
                item.setDateOfDiagnosis(dateDiag);
                dao.create(item);
                long dateHba1=myCalendar.getTimeInMillis();
                java.sql.Date dateHba=new java.sql.Date(dateHba1);
                long dateOth=myCalendar1.getTimeInMillis();
                java.sql.Date dateOther=new java.sql.Date(dateOth);
                long datePh=myCalendar2.getTimeInMillis();
                java.sql.Date datePhy=new java.sql.Date(datePh);

                item1.setHba1(viewHolder.hba.getText().toString()+"%");
                item1.setCholes(viewHolder.choles.getText().toString()+"mmol/L");
                item1.setHdl(viewHolder.hdl.getText().toString()+"mmol/L");
                item1.setLdl(viewHolder.ldl.getText().toString()+"mmol/L");
                item1.setTrig(viewHolder.trig.getText().toString()+"mmol/L");
                item1.setDateHba(dateHba);
                item1.setDateOther(dateOther);
                item1.setPhysicalTest(datePhy);
                item1.setPatientId(Long.toString(item.getId()));
                dao1.create(item1);
                String reference = "BCU"+Long.toString(item.getId())+date.toString().replace("-","");
                item.setReference(reference);
                dao.update(item);
                Toast.makeText(this, "Patient Registered Successfully", Toast.LENGTH_LONG).show();
                startActivity(new Intent(AddPatientActivity.this,MainActivity.class));
            }
        }catch (Exception e){}
    }
    private void updateLabel() {
        viewHolder.date.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateLabel1() {
        viewHolder.date1.setText(sdf.format(myCalendar1.getTime()));
    }
    private void updateLabel2() {
        viewHolder.physical.setText(sdf.format(myCalendar2.getTime()));
    }
    private void updateLabel3() {
        viewHolder.dateDiag.setText(sdf.format(myCalendar3.getTime()));
    }
}
