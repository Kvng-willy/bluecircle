package com.datex.datexapp.datex.Pages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.DbObjects.Results;
import com.datex.datexapp.datex.Fragments.ProfileFragment;
import com.datex.datexapp.datex.Fragments.ResultFragment;
import com.datex.datexapp.datex.R;
import com.datex.datexapp.datex.Widgets.Utility;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    public DbService dbService;
    public Dao<Results,Long> dao;
    public List<Results> items;
    public Dao<Patient,Long> dao1;
    public List<Patient> patients;
    public Utility utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            dbService = new DbService(DetailActivity.this).getInstance(DetailActivity.this);
            dao = DaoManager.createDao(dbService.getConnectionSource(), Results.class);
            dao1 = DaoManager.createDao(dbService.getConnectionSource(), Patient.class);
            utils = new Utility(DetailActivity.this);
            TabLayout tabLayout = findViewById(R.id.tabs);
            tabLayout.addTab(tabLayout.newTab().setText("Home"));
            tabLayout.addTab(tabLayout.newTab().setText("All Jobs"));
            mViewPager = findViewById(R.id.container);
            tabLayout.setupWithViewPager(mViewPager);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(android.R.color.holo_blue_dark));
            mViewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount()));
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.getTabAt(1).setText("Test Results");
            tabLayout.getTabAt(0).setText("Profile");
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    mViewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                }
            });

        }catch (Exception e){}
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if(id==R.id.action_search1){
            new AlertDialog.Builder(DetailActivity.this)
                    .setTitle("Delete")
                    .setMessage("Are you sure you want to delete this profile?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            try {
                                QueryBuilder<Results,Long> queryBuilder = dao.queryBuilder().orderBy("Id",false);
                                queryBuilder.where().like("patientId",utils.getPref().getLong("Id",0));
                                items = queryBuilder.query();
                                for(Results item:items){
                                    dao.delete(item);
                                }
                                QueryBuilder<Patient,Long> queryBuilder1 = dao1.queryBuilder().orderBy("Id",false);
                                queryBuilder1.where().like("Id",utils.getPref().getLong("Id",0));
                                patients = queryBuilder1.query();
                                dao1.delete(patients.get(0));
                                Toast.makeText(DetailActivity.this, "Profile Deleted", Toast.LENGTH_SHORT).show();
                                finish();
                                startActivity(new Intent(DetailActivity.this,MainActivity.class));
                                } catch (Exception e) {
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new ProfileFragment();
                case 1:
                    return new ResultFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

}
