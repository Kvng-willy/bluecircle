package com.datex.datexapp.datex.DbObjects;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.datex.datexapp.datex.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;


/**
 * Created by KVNG_WILLY on 05/03/2018.
 */

public class DbService extends OrmLiteSqliteOpenHelper {
    private  static final String DATABASE_NAME ="Datax.db";
    private static final int DATABASE_VERSION = 1;
    public DbService(Context context){super(context,DATABASE_NAME,null,DATABASE_VERSION, R.raw.orm_config);}
    public DbService getInstance(Context context){return  new DbService(context);}
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try{
            TableUtils.createTableIfNotExists(connectionSource,Patient.class);
            TableUtils.createTableIfNotExists(connectionSource,Results.class);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
        try {
            TableUtils.dropTable(connectionSource,Patient.class,true);
            TableUtils.dropTable(connectionSource,Results.class,true);
            onCreate(sqLiteDatabase);
        }catch(Exception e){}
    }
}
