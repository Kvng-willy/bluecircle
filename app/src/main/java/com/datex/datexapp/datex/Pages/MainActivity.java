package com.datex.datexapp.datex.Pages;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bvapp.arcmenulibrary.ArcMenu;
import com.datex.datexapp.datex.DbObjects.DbService;
import com.datex.datexapp.datex.DbObjects.Patient;
import com.datex.datexapp.datex.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    PieChart pieChart;
    public MyViewHolder viewHolder;
    public DbService dbService;
    public Dao<Patient,Long> dao;
    public List<Patient> items;
    public int gesta;
    public int type1;
    public int type2;

    public class MyViewHolder{
        public TextView all,gdm,male,female;
        public ImageButton addpatient,message;
        public LinearLayout allPatients;
        public MyViewHolder(){
            all = findViewById(R.id.all);
            gdm = findViewById(R.id.gdm);
            male = findViewById(R.id.male);
            female = findViewById(R.id.female);
            addpatient = findViewById(R.id.addPatient);
            message = findViewById(R.id.message);
            allPatients = findViewById(R.id.allpatients);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            dbService = new DbService(MainActivity.this).getInstance(MainActivity.this);
            dao = DaoManager.createDao(dbService.getConnectionSource(),Patient.class);
            viewHolder = new MyViewHolder();

           /* FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, AddPatientActivity.class));
                }
            });*/

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);


            GetAllPatient();
            GetGestational();
            GetMale();
            GetFemale();
            GetType1();
            GetType2();
            pieChart = findViewById(R.id.piechart);
            pieChart.setUsePercentValues(true);
            setUpPieChart();
            viewHolder.addpatient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this,AddPatientActivity.class));
                }
            });
            viewHolder.message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this,MessageActivity.class));
                }
            });
            viewHolder.allPatients.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this,PatientsActivity.class));
                }
            });
        }catch (Exception e){}
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.action_search1);

        SearchView sv = (SearchView)item.getActionView();
        ((EditText) sv.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(getResources().getColor(R.color.colorWhite));
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    startActivity(new Intent(MainActivity.this, SearchActivity.class).putExtra("query", query));
                }catch (Exception e){}
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {return false;}
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            finish();
            startActivity(new Intent(MainActivity.this,MainActivity.class));
            // Handle the camera action
        } else if (id == R.id.addPatient) {
            startActivity(new Intent(MainActivity.this,AddPatientActivity.class));
        } else if (id == R.id.allpatients) {
            startActivity(new Intent(MainActivity.this,PatientsActivity.class));
        } else if (id == R.id.newEntries) {
            startActivity(new Intent(MainActivity.this,WeeklyEntryActivity.class));
        }else if (id == R.id.notify) {
            startActivity(new Intent(MainActivity.this,NotificationActivity.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void setUpPieChart() {
        ArrayList<PieEntry> aList = new ArrayList<>();
        aList.add(new PieEntry(type1, "Type 1"));
        aList.add(new PieEntry(type2, "Type 2"));
        aList.add(new PieEntry(gesta, "Gestational"));

        PieDataSet dataSet = new PieDataSet(aList, "Color Code");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(dataSet);

        Description description = new Description();
        description.setText("Registered Diabetic Patient Diagnosis");

        pieChart.setDescription(description);
        pieChart.setDrawHoleEnabled(false);

        pieChart.setData(data);
        pieChart.invalidate(); //refresh

        //set the formatter for the pie Chart
    }
    public void GetAllPatient(){
        try {
            items = dao.queryForAll();
            viewHolder.all.setText(Integer.toString(items.size()));
        }catch (Exception e){}

    }
    public void GetGestational(){
        try {
            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("diagnosis","Gestational DM");
            items = queryBuilder.query();
            viewHolder.gdm.setText(Integer.toString(items.size()));
            gesta = items.size();
        }catch (Exception e){}

    }
    public void GetType1(){
        try {
            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("diagnosis","Type 1");
            items = queryBuilder.query();
            type1 = items.size();
        }catch (Exception e){}

    }
    public void GetType2(){
        try {
            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("diagnosis","Type 2");
            items = queryBuilder.query();
            type2 = items.size();
        }catch (Exception e){}

    }
    public void GetMale(){
        try {
            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("gender","Male");
            items = queryBuilder.query();
            viewHolder.male.setText(Integer.toString(items.size()));
        }catch (Exception e){}

    }
    public void GetFemale(){
        try {
            QueryBuilder<Patient,Long> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("gender","Female");
            items = queryBuilder.query();
            viewHolder.female.setText(Integer.toString(items.size()));
        }catch (Exception e){}

    }
}
